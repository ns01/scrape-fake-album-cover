#!/usr/bin/python

# Scrape A Fake Album Cover - Scrape random photo and words to create a fake album cover.
# Copyright (C) 2019 Nick Santos

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import requests
from bs4 import BeautifulSoup
from PIL import Image, ImageDraw, ImageFont, ImageEnhance

response_obj = requests.get('https://picsum.photos/g/420/?random')
name = 'random_image.png'

with open(name, 'wb') as raw_file:
    raw_file.write(response_obj.content)

img = Image.open('random_image.png')
draw = ImageDraw.Draw(img)
draw.rectangle(((0, 15), (210, 52)), fill='black')
draw.rectangle(((170,350), (450, 380)), fill='black')

band_name_font = ImageFont.truetype('/usr/share/fonts/noto/NotoSans-Bold.ttf', 25)
album_title_font = ImageFont.truetype('/usr/share/fonts/cantarell/Cantarell-Regular.otf', 20)

response_random_word = requests.get('https://randomword.com')
soup = BeautifulSoup(response_random_word.content, 'html.parser')
find_word = soup.find('div', id='random_word')
random_word = find_word.get_text()
band_name = random_word 

r_random_phrase = requests.get('https://randompassphrasegenerator.com')
soup2 = BeautifulSoup(r_random_phrase.content, 'html.parser')
random_phrase = soup2.find('h1', id='thePhrase').get_text()
album_title = random_phrase

draw.text((20,15), band_name, (250,250,250), font = band_name_font)
draw.text((180,350), album_title, (250,250,250), font = album_title_font)
img.save('fake_album_cover.jpg')

